## Too many results in published scientific papers are not reproducible

- The data is unavailable <!-- .element: class="fragment roll-in" -->
- The environment no longer exists <!-- .element: class="fragment roll-in" -->
- <!-- .element: class="fragment roll-in" --> <span class="fragment highlight-red">The code is broken!</span>