## Why doesn't the code build?

- Missing dependencies <!-- .element: class="fragment roll-in" -->
- Mysterious configuration parameters <!-- .element: class="fragment roll-in" -->
- Compile errors <!-- .element: class="fragment roll-in" -->
- And of course, there is no documentation <!-- .element: class="fragment roll-in" -->