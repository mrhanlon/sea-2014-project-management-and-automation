### Reproducibility in Computer Science

![Measuring Reproducibility in Computer Systems Research](images/reproducibility-cs.png) <!-- .element: style="width: 600px; background: #999" class="fragment fade-out" data-fragment-index="1" -->

# That's only a 20% success rate! <!-- .element: style="position: relative;  top: -400px" class="fragment roll-in" data-fragment-index="1" -->

<!-- .element: style="position: relative;  top: -200px" --> \[ source: [http://reproducibility.cs.arizona.edu/](http://reproducibility.cs.arizona.edu/) \]