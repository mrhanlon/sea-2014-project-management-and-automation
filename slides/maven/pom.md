# POM.xml

## The _Project Object Model_

|||
|-|-|
|_Project information_ | Name, version, URL, developers, contributors, licensing, organization|
|_Build settings_ | Build process, dependencies, non-code resource handling, reporting|
|_Environment settings_ | Source control, repositories, distribution, issue management|
