## Get up and running in minutes

```
$> git clone <repo url> my-dev-env
$> cd my-dev-env
$> git submodule init
$> git submodule update
$> mvn install
$> mvn liferay:deploy
```
