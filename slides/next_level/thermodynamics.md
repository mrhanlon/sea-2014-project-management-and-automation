## The Second Law of Thermodynamics

> The entropy of an isolated system never decreases, because isolated systems always evolve toward thermodynamic equilibrium a state with maximum entropy.
