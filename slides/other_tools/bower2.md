## Declarative JSON configuration

```
{
  "name": "vislab-reservation-portlet",
  "version": "1.0.0",
  "dependencies": {
    "modernizr": "~2.6.2",
    "jqueryui-timepicker-addon": "1.4.3",
    "mustache": "~0.8.1",
    "datejs": "*",
    "fullcalendar": "~1.6.4"
  }
}
```
