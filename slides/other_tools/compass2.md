```
@mixin box-sizing($bs) {
  $bs: unquote($bs);
  @include experimental(box-sizing, $bs,
    -moz, -webkit, not -o, not -ms, not -khtml, official
  );
}

* {
  @include box-sizing(border-box);
}

.alert {
  border: 1px solid black;
}

.alert-error {
  @extend .alert;
  color: red;
  border-color: red;
}
```
