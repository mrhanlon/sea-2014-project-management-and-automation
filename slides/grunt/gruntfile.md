# Gruntfile.js

### Declarative JSON/JavaScript configuration file

### Executes tasks in Node.js runtime

### Loads of community plugins:

- jslint/jshint
- file tasks
- watch
- compass
- library/framework support
